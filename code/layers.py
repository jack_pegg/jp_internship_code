import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.regularizers import l2 


# ========== LAYER FNS ========== #

def Conv2D(ch, ksize, strides, padding='same', bias=True, init='glorot_uniform',reg=0.001):
	return keras.layers.Conv2D(ch, ksize, strides, padding=padding, use_bias=bias, kernel_initializer=init, kernel_regularizer=l2(reg))

def UpSample(output_size):
	return keras.layers.Lambda(lambda x: tf.compat.v1.image.resize(x, output_size, align_corners=True))

def Deconv(ch, ksize, strides, bias=True):
	return keras.layers.Conv2DTranspose(ch, ksize, strides, padding='same', use_bias=bias)

def Dense(units, use_bias=True, reg=0.001):
	return keras.layers.Dense(units, use_bias=use_bias, kernel_regularizer=l2(reg))

def MaxPool(size=(2,2), strides=2, padding='valid'):
	return keras.layers.MaxPool2D(pool_size=size,strides=strides,padding=padding)

def AvePool(size=(2,2), strides=2, padding='valid'):
	return keras.layers.AveragePooling2D(pool_size=size,strides=strides,padding=padding)

# used in upscale block below, interleaves 4 filters into 1 big filter
def Interleave(filters):
	
	f1,f2,f3,f4 = filters
	for idx,f in enumerate(filters):
		input_shape = f.get_shape().as_list()
		if idx != 0:
			assert input_shape == old_input_shape
		assert len(input_shape) == 4
		old_input_shape = input_shape
	
	size = input_shape[1]
	[f1,f2,f3,f4] = [tf.split(f,size,axis=2) for f in filters]
	
	for i,j in zip(f1,f2):
		out = tf.concat([i,j],axis=2)
		try:
			tmp1 = tf.concat([tmp1,out],axis=2)
		except:
			tmp1 = out

	for i,j in zip(f3,f4):
		out = tf.concat([i,j],axis=2)
		try:
			tmp2 = tf.concat([tmp2,out],axis=2)
		except:
			tmp2 = out
	
	tmp1, tmp2 = tf.split(tmp1,size,axis=1), tf.split(tmp2,size,axis=1)

	for i,j in zip(tmp1,tmp2):
		out = tf.concat([i,j],axis=1)
		try:
			interleaved = tf.concat([interleaved,out],axis=1)
		except:
			interleaved = out
	
	return interleaved


# ========== PRIMARY LAYER CLASSES ========== #

# novel upscale block used in paper Laina et al: Monocular Depth Perception
# good results but way more computationally expensive than is necessary
class UpscaleRelu(keras.layers.Layer):

	def __init__(self,ch):
		super(UpscaleRelu,self).__init__()
		self.conv1_1 = Conv2D(ch, (3,3), 1)
		self.conv1_2 = Conv2D(ch, (2,3), 1)
		self.conv1_3 = Conv2D(ch, (3,2), 1)
		self.conv1_4 = Conv2D(ch, (2,2), 1)

		self.conv2 = Conv2D(ch, (3,3), 1)
		self.relu = keras.layers.ReLU()

		self.conv3_1 = Conv2D(ch, (3,3), 1)
		self.conv3_2 = Conv2D(ch, (2,3), 1)
		self.conv3_3 = Conv2D(ch, (3,2), 1)
		self.conv3_4 = Conv2D(ch, (2,2), 1)

	def call(self,x):
		conv1_1 = self.conv1_1(x)
		conv1_2 = self.conv1_2(x)
		conv1_3 = self.conv1_3(x)
		conv1_4 = self.conv1_4(x)
		inter1 = Interleave([conv1_1,conv1_2,conv1_3,conv1_4])
		relu1 = self.relu(inter1)
		conv2 = self.conv2(relu1)

		conv3_1 = self.conv3_1(x)
		conv3_2 = self.conv3_2(x)
		conv3_3 = self.conv3_3(x)
		conv3_4 = self.conv3_4(x)
		inter3 = Interleave([conv3_1,conv3_2,conv3_3,conv3_4])

		output = tf.add(conv2,inter3)
		output = self.relu(output)

		return output


class ConvReluBN(keras.layers.Layer):

	def __init__(self, ch, ksize, strides, padding='same', slope=0, reg=0.001):
		super(ConvReluBN,self).__init__()
		self.out = keras.Sequential([
			Conv2D(ch,ksize,strides,padding=padding,bias=False,reg=reg),
			keras.layers.ReLU(negative_slope=slope),
			keras.layers.BatchNormalization()
			])

	def call(self, x, training=None):
		return self.out(x, training=training)


class DenseReluBN(keras.layers.Layer):

	def __init__(self, units, slope=0, reg=0.001):
		super(DenseReluBN,self).__init__()
		self.out = keras.Sequential([
			Dense(units, use_bias=False, reg=reg),
			keras.layers.ReLU(negative_slope=slope),
			keras.layers.BatchNormalization()
			])

	def call(self, x, training=None):
		return self.out(x, training=training)


# Dense layer to correct total number of units based on out_shape, then reshape tensor to out_shape
class Dense2Conv(keras.layers.Layer):

	def __init__(self, out_shape=[8,8,32], slope=0):
		super(Dense2Conv,self).__init__()
		self.out_shape = out_shape
		self.decode1 = DenseReluBN(256,slope=slope)
		self.decode2 = DenseReluBN(out_shape[0]*out_shape[1]*out_shape[2],slope=slope)
		
	def call(self, x, training=None):
		decode1 = self.decode1(x, training=training)
		decode2 = self.decode2(decode1, training=training)

		return tf.reshape(decode2,[-1,self.out_shape[0], self.out_shape[1], self.out_shape[2]])

