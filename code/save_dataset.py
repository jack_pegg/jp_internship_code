import utils
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--file_path', help="File path to source ",
						 type=str)
	parser.add_argument('--save_path', help="File path to save location ",
						 type=str,default='../datasets')
	parser.add_argument('--strides', help="stride between patches ",
						 type=int,default=70)
	parser.add_argument('--tb', help="testbox being used ",
						 type=int,default=6)
	parser.add_argument('--use_every', help="use every _ image ",
						 type=int,default=5)
	parser.add_argument('--patch_size', help="size of patches ",
						 type=int,default=90)
	parser.add_argument('--col', help="save colour patches? ",
						 type=bool,default=False)
	parser.add_argument('--oneshot', help="input whole image at once? ",
					type=bool,default=False)
	
	args = parser.parse_args()
	
	utils.save_dataset(args.file_path, args.save_path, args.strides, args.tb, args.use_every, args.patch_size, args.col, args.oneshot)
