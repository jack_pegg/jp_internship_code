import tensorflow as tf
from tensorflow import keras
import numpy as np
import argparse
import utils
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import cv2
import os
import shutil

from tensorflow.contrib.tensorboard.plugins import projector


parser = argparse.ArgumentParser()

parser.add_argument('--model_name', help="Model to import ",
					 type=str,default='VAE_4x4')
parser.add_argument('--testbox', help="which dataset is being used",
					 type=int, default=6)
parser.add_argument('--latent_size', help="latent_size ",
					 type=int,default=32)
parser.add_argument('--out_size', help="number of output channels ",
					 type=int,default=1)
parser.add_argument('--reshape_size', help="number of output channels ",
					 type=int,default=64)
parser.add_argument('--reg', help="weight regularisation coeff ",
					 type=float,default=0.002)
parser.add_argument('--oneshot', help="input whole image at once? ",
					type=int,default=0)

args = parser.parse_args()


if args.model_name == 'AE_4x4':
	from models import AE_4x4 as tf_model

if args.model_name == 'VAE_4x4':
	from models import VAE_4x4 as tf_model

if args.model_name == 'VAE_1x1':
	from models import VAE_1x1 as tf_model

col = 1 if args.out_size == 3 else 0
load_name = '../datasets/train_data_tb{}_c{}_os{}.npz'.format(args.testbox,col,args.oneshot)
folder_name = '{}_{}_{}_{}'.format(args.model_name,args.latent_size,args.out_size,args.oneshot)
logdir = "../logs/{}/projections".format(folder_name)


def prepro_batch(x,size=None):
	
	if len(x.get_shape().as_list()) != 4:
		x = tf.expand_dims(x,-1)
	x = utils.resize_tensor(x,(args.reshape_size,args.reshape_size)) / 255.

	return x


# function to create sprite from each patch that can be used by tensorboard to display the points in the PCA/TSNE/UMAP
# plots as their corresponding patches
def images_to_sprite(data):
	
		if len(data.shape) == 3:
			data = np.tile(data[...,np.newaxis], (1,1,1,3))
		data = data.astype(np.float32)
		min = np.min(data.reshape((data.shape[0], -1)), axis=1)
		data = (data.transpose(1,2,3,0) - min).transpose(3,0,1,2)
		max = np.max(data.reshape((data.shape[0], -1)), axis=1)
		data = (data.transpose(1,2,3,0) / max).transpose(3,0,1,2)
		

		n = int(np.ceil(np.sqrt(data.shape[0])))
		padding = ((0, n ** 2 - data.shape[0]), (0, 0),
				(0, 0)) + ((0, 0),) * (data.ndim - 3)
		data = np.pad(data, padding, mode='constant',
				constant_values=0)
		
		# Tile the individual thumbnails into an image.
		data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3)
				+ tuple(range(4, data.ndim + 1)))
		data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
		data = (data * 255.).astype(np.uint8)
		
		return data


# get lvec from patch
def get_feature_vec(model, image):
	try:
		lat,_ = model(image, training=False)
		size = lat.get_shape().as_list()[1]
	except:
		[lat,_,_],_ = model(image, training=False)
		size = lat.get_shape().as_list()[1]

	if size != 1:
		lat = keras.layers.AveragePooling2D(pool_size=size)(lat)
		lat = keras.layers.Flatten()(lat)

	return lat


# just a few image patches that cover a variety of different content so when they can be concatenated onto the random sample of patches used
# to guarantee the positions of all different types of patch can be seen in the latent space
def get_patches():
	
	def get_sorted_seq(image_folder):

		def halfsize(x):
			return cv2.resize(x,(0,0),fx=0.5,fy=0.5)
		
		return [[halfsize(cv2.imread(os.path.join(image_folder,p),0)),p] for p in sorted(os.listdir(image_folder))]	
	
	image_sequence = get_sorted_seq('../datasets/testbox6')
	path_patches = [(51,512,1200),(3,504,1243),(3,600,852),(92,256,760),(78,550,780)]
	patches = []
	
	for v in path_patches:
		patch = image_sequence[v[0]][0][int(v[1]-45):int(v[1]+45),int(v[2]-45):int(v[2]+45)]
		patch = np.expand_dims(patch,0)
		patches.append(patch)

	return np.concatenate(patches,0)


def main():

	if os.path.exists(logdir):
		shutil.rmtree(logdir)
	
	emb_size = 35 # choose number of features

	# get sample of image patches
	x = np.load(load_name)["stacks"]
	x = np.concatenate(x,axis=0)
	x = shuffle(x)
	x_sample = x[:emb_size]
	refs = get_patches()
	x_sample = np.concatenate([x_sample,refs],axis=0)

	# get lvecs
	with tf.Session().as_default() as sess:
		model = tf_model(args.latent_size,args.out_size,args.reg)
		model.build(input_shape=(emb_size,args.reshape_size,args.reshape_size,1))
		path = '../saves/{}/AE_checkpoint.h5'.format(folder_name)
		model.load_weights(path)

		xt = tf.convert_to_tensor(x_sample)
		xt = prepro_batch(xt)
		lvecs = (get_feature_vec(model, xt)).eval()

	# create tf variable to save for the embeddings
	features = tf.Variable(lvecs, name='features')
	# save sprite image
	sprite = images_to_sprite(x_sample)
	cv2.imwrite(os.path.join(logdir, 'sprite.png'), sprite)
	
	with tf.Session() as sess:

		# initialize variables and Tensorboard Filewriter:
		sess.run(tf.global_variables_initializer())
		writer = tf.summary.FileWriter(logdir, sess.graph)

		# save embeddings
		feature_saver = tf.train.Saver([features], max_to_keep=1)
		update_embeddings = tf.assign(features, lvecs)
		sess.run(update_embeddings)
		feature_saver.save(sess, os.path.join(logdir, 'features.ckpt'))
		
		# save sprite again (not sure why but without doing this the image doesn't save)
		cv2.imwrite(os.path.join(logdir, 'sprite.png'), sprite)

		# setup and save tensorboard projections
		config = projector.ProjectorConfig()
		embedding = config.embeddings.add()
		embedding.tensor_name = features.name
		embedding.sprite.image_path = 'sprite.png'
		embedding.sprite.single_image_dim.extend([x_sample.shape[1], x_sample.shape[1]])
		projector.visualize_embeddings(writer, config)


if __name__ == '__main__':
	main()