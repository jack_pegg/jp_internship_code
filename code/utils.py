import numpy as np
import cv2
import sys
import os
from os.path import join
import argparse
import matplotlib.pyplot as plt
import matplotlib.colors as mpcolors
from matplotlib import cm
import pylab
from mpl_toolkits.mplot3d import Axes3D, axes3d

from PIL import Image
from PIL import ImageFilter
from tqdm import tqdm
import shutil

import umap

from scipy import ndimage
from scipy.ndimage import gaussian_laplace
from skimage import transform as tfrm
from sklearn.utils import shuffle

import tensorflow as tf
from tensorflow import keras

import sklearn
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture
from scipy.spatial.distance import cosine


# scale array to 0,1 float or 0,255 uint8, useful for viewing matplotlib plots
def scale(img,uint_scale=True):
	X = ((img - np.min(img)) / (np.max(img) - np.min(img)))
	if uint_scale:
		return np.uint8(255*X)
	else:
		return X


# cvt grayscale img to rgb
def grey2rgb(img):
	img = np.squeeze(img)
	return np.stack([img,img,img],axis=-1)


# resize image tensor
def resize_tensor(tensor, size):
	return tf.compat.v1.image.resize(tensor,size,align_corners=True)


# resize and normalise tensor
def prepro(x,oneshot,size=None,contrast=False):
	x = tf.convert_to_tensor(x, dtype=tf.float32) 
	# make 4dimensional
	if len(x.shape) == 3:
		x = tf.expand_dims(x,-1)
	if len(x.shape) == 2:
		x = tf.expand_dims(x,-1)
		x = tf.expand_dims(x, 0)		
	if size is not None:
		x = resize_tensor(x,size)
	else:
		# slightly downsample, patch size 90->64 is same fraction as 1024,1536->728,1092
		if oneshot:
			x = resize_tensor(x,(728,1096))
		else:
			x = resize_tensor(x,(64,64))

	# used for autoencoder going from grey to colour images to exaggerate difference in colour between maggots, dirt etc.
	if contrast:
		x = tf.image.adjust_contrast(x,1.5)
	
	return x / 255.


# halve image size
def halfsize(x):
	return cv2.resize(x,(0,0),fx=0.5,fy=0.5)


# cvt to spherical coordinates, was tried when visualising latent vectors, not currently used
def spherical(lvecs):
    ptsnew = np.zeros(lvecs.shape)
    xy = lvecs[:,0]**2 + lvecs[:,1]**2
    ptsnew[:,0] = np.sqrt(xy + lvecs[:,2]**2) # distance from origin
    ptsnew[:,1] = np.arctan2(np.sqrt(xy), lvecs[:,2]) # for elevation angle defined from Z-axis down
    ptsnew[:,2] = np.arctan2(lvecs[:,1], lvecs[:,0]) # angle perpendicular to z axis
    return ptsnew


# rotate and flip patches for data augmentation
def rotate_and_flip(img):
	tmp = []
	for i in range(2):
		for j in range(4):
			if i == 1:
				im = cv2.flip(img,0)
			else:
				im = img
			h,w = im.shape[:2]
			center = (w/2,h/2)
			M = cv2.getRotationMatrix2D(center,j*90,1.0)
			im = cv2.warpAffine(im, M, (h,w))
			if (len(im.shape) == 2) or  (im.shape[0] != 1):
				im = np.expand_dims(im,axis=0)
			tmp.append(im)
	assert len(tmp) == 8

	return np.concatenate(tmp, axis=0)


# perform sift on image
def SIFT(image_path,save=False):

	x = cv2.imread(image_path,0)
	x = cv2.resize(x, (0,0), fx=0.25, fy=0.25)
	sift = cv2.xfeatures2d.SIFT_create()
	kp = sift.detect(x,None)
	out = cv2.drawKeypoints(x,kp,x.copy(),flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
	kp,des = sift.compute(x,kp)
	if save:
		cv2.imwrite('../datasets/sift-{}.jpg'.format(idx),out)

	return kp, des


# change image gamme level
def adjust_gamma(image, gamma=1.0):
	
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
 
	return cv2.LUT(image, table)


# sort pairs of images in otder, only used for dimensionality reduction plots
def sort_pairs(sub_li): 
	l = len(sub_li) 
	for i in range(0, l): 
		for j in range(0, l-i-1): 
			if (sub_li[j][1] > sub_li[j + 1][1]): 
				tempo = sub_li[j] 
				sub_li[j]= sub_li[j + 1] 
				sub_li[j + 1]= tempo 
	return sub_li 


# tile row of n**2 images to a nxn grid
def tile_images(images,size):
	x = np.concatenate(np.squeeze(images),axis=1)
	x = np.split(x, size, axis=1)
	return np.concatenate(x, axis=0)


# load dataset, cvt to tf.Dataset, shuffle, and get n_batches for training loop
def get_BSF_dataset(batch_size, file_path):

	x = np.load(file_path)["stacks"]
	x = np.concatenate(x,axis=0)
	x = shuffle(x)
	xs = tf.convert_to_tensor(x, dtype=tf.float32)
	db = tf.data.Dataset.from_tensor_slices((xs))
	db = db.batch(batch_size).shuffle(2048)

	return db, np.ceil(x.shape[0] / batch_size)


# get image patches from image
def get_patches(image, patch_size=90, strides=90, aug=True): 
	tmp = []
	for i in range( int(np.ceil(image.shape[0] / strides) - np.floor(patch_size / strides)) ): # row idx
		for j in range( int(np.ceil(image.shape[1] / strides) - np.floor(patch_size / strides)) ): # col idx
			patch = image[(strides*i):(strides*i)+patch_size,(strides*j):(strides*j)+patch_size] # get patch
			if patch.shape[0] != patch_size: # if it's too short, i.e. patch goes off bottom edge
				if patch.shape[1] != patch_size: # if it's too thin, i.e. patch goes off right edge
					patch = image[-patch_size:,-patch_size:] # get new patch at right-bottom corner of correct size
				else:
					patch = image[-patch_size:,(strides*j):(strides*j)+patch_size] # get new patch at bottom edge of correct size
			else:
				if patch.shape[1] != patch_size:
					patch = image[(strides*i):(strides*i)+patch_size,-patch_size:] # get new patch at right edge of correct size
			
			# hard coded for patches to be size 90 at the moment, change if necessary
			if patch_size != 90:
				patch = cv2.resize(patch,(patch_size,patch_size))
			if aug == True:
				patch = rotate_and_flip(patch)
				assert (patch.shape == (8,patch_size,patch_size)) or (patch.shape == (8,patch_size,patch_size,3))

			if (len(patch.shape) == 2):
				patch = np.expand_dims(patch,axis=0)
			
			tmp.append(patch)
	
	patch_stack = np.concatenate(tmp,axis=0)
	return patch_stack


# save dataset of patches from all images as npz
def save_dataset(file_path, save_path, strides, tb, use_every=None, patch_size=90, col=False,oneshot=False):
	
	print('saving...')
	stacks = []
	cropsize = 200 # border to exclude from image i.e. plastic tray walls
	print(len(os.listdir(file_path)))

	if use_every is not None:
		if col == False:
			im_subset = [f for idx,f in enumerate(sorted(os.listdir(file_path))) if (idx+1)%use_every==0 and '.png' in f]
		else:
			im_subset = [f for idx,f in enumerate(sorted(os.listdir(file_path))) if (idx+1)%use_every==0 and '.jpg' in f]
	else:
		im_subset = os.listdir(file_path)

	print(len(im_subset))
	
	for idx,f in enumerate(shuffle(im_subset)):
		if col:
			im = cv2.imread(join(file_path,f))
			im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
		else:
			im = cv2.imread(join(file_path,f),0)
		im = halfsize(im)
		im = im[cropsize:-cropsize,cropsize:-cropsize]
		try:
			patch_stack = get_patches(im, patch_size, strides)
		except:
			pass
		stacks.append(patch_stack)
		print(patch_stack.shape)
				
	print(len(stacks))
	np.savez(join(save_path,'train_data_tb{}_c{}_os{}.npz'.format(tb,int(col),int(oneshot))), stacks=stacks)


# get latent vec from image patch 
def patch_to_lvec(x,model,lsize,oneshot=True,pool_size=2):
	try:
		[_,_,lat],_ = model(x, training=False)
		size = int(lat.numpy().shape[1])
	except:
		lat,_ = model(x, training=False)
		size = int(lat.numpy().shape[1])
	
	if oneshot: # if whole image, pool by pool_size, then reshape to [X,lsize] where X will depend on the pool size
		lat = keras.layers.AveragePooling2D(pool_size)(lat)
		sh = lat.shape
		lat = tf.reshape(lat,[sh[1]*sh[2],lsize])
	else: # if a patch, pool so lvec is 1x1xlsize, then Flatten to [,lsize]
		lat = keras.layers.AveragePooling2D(pool_size=4)(lat) #4x4xL -> 1x1xL for patch
		sh = lat.shape
		lat = keras.layers.Flatten()(lat)
	return lat.numpy(), sh


# get PCA model for lvecs
def get_PCA(lvecs, n_dim):
	pca = PCA(n_components=n_dim)
	X = pca.fit_transform(lvecs)
	return X, pca


# get kmeans model for lvecs
def get_kmeans(lvecs, K):
	kmeans = KMeans(n_clusters=K, max_iter=1000,algorithm='auto')
	labels = kmeans.fit_predict(lvecs)
	return labels, kmeans


# get GMM model for lvecs
def get_GMM(lvecs, K, cov='full'):
	gmm = GaussianMixture(n_components=K)
	gmm.fit(lvecs)
	labels = np.float32(gmm.predict_proba(lvecs))
	return labels, gmm


# plot 2D PCA or GMM
def fit_and_plot_2d(lvecs, method, filename):
	if method == 'pca':
		X, _ = get_PCA(lvecs,n_dim=2)
	if method == 'gmm':
		X,_ = get_GMM(lvecs,K=2)
	
	plt.figure(figsize = (10,8))
	ax = plt.gca()
	ax.axis('equal')
	ax.scatter(X[:, 0], X[:, 1], s=20, cmap='viridis', zorder=2)
	plt.savefig('{}'.format(filename))
	plt.close()


# plot 3D PCA or GMM
def fit_and_plot_3d(lvecs, method, filename, cov='full'):
	if method == 'pca':
		X, _ = get_PCA(lvecs,n_dim=3)
	if method == 'gmm':
		X,_ = get_GMM(lvecs,K=3,cov=cov)
	else:
		X = lvecs

	fig = pylab.figure()
	ax = fig.add_subplot(111,projection='3d')
	ax.scatter(X[:, 0], X[:, 1], X[:, 2], s=20, cmap='viridis', zorder=2)
	plt.savefig('{}'.format(filename))
	plt.close()


# test kmeans with range of K values
def fit_and_plot_kmeans_range(lvecs, images, filename):

	for K in range(3,8,1):
		labels, kmeans = get_kmeans(lvecs,K)
		image_dict = {}
		images = np.squeeze(images)

		for idx,im in enumerate(images):
			_id = labels[idx]
			if _id in image_dict:
				if len(image_dict[_id]) < 25:
					image_dict[_id].append(im)
			else:
				image_dict[_id] = [im]

		plt.figure()
		for k,v in image_dict.items():
			tile = tile_images(v,size=5)
			plt.subplot(1,K,int(k+1))
			plt.imshow(tile,cmap='gray')
		plt.axis('off')
		plt.savefig('{}-{}'.format(filename,K))
		plt.close()


# sample images and plot GMM patch
def plot_GMM(lvecs, images, filename):
	labels,_ = get_GMM(lvecs, K=3)
	pairs = [(images[i],labels[i]) for i in range(36)]
	pairs = shuffle(pairs)

	fig = plt.figure()
	for idx,p in enumerate(pairs):
		ones = np.ones((64,64,3))
		patch = np.multiply(p[1],ones,out=ones,casting='unsafe')
		image = grey2rgb(p[0])
		tile = np.concatenate([patch,image],axis=1)
		plt.subplot(6,6,idx+1)
		plt.imshow(tile)
		plt.axis('off')
	plt.savefig('{}'.format(filename))
	plt.close()


# plot pairs of patches and cos distances between their latent vectors
def plot_distances(lvecs, images, filename, size=5):
	plt.figure()
	data = []
	for i in range(images.shape[0]):
		idx1,idx2 = np.random.choice(lvecs.shape[0],size=2,replace=False)
		dist = cosine(lvecs[idx1],lvecs[idx2])
		pair = np.squeeze(np.concatenate([images[idx1],images[idx2]],axis=1))
		data.append([pair,dist])
	
	data = sort_pairs(data)
	
	for i in range(size**2):
		plt.subplot(size,size,i+1)
		plt.imshow(data[i][0],cmap='gray')
		plt.axis('off')
		plt.title("{0:.2f}".format(dist))
	plt.savefig('{}'.format(filename))
	plt.close()


# do same but for GMM vectors
def plot_GMM_distances(lvecs, images, filename, size=5, maxdim=32, step=5):
	for k in range(3,maxdim,step):
		labels, _ = get_GMM(lvecs,K=k)
		plot_distances(lvecs, images, filename + '-{}'.format(k))


# get dimensions of heatmap image given tiling or original image with patch_size and strides
def get_row_col_lengths(im,strides,patch_size):
	row_length = int(np.ceil(im.shape[1] / strides) - np.floor(patch_size / strides) )
	col_length = int(np.ceil(im.shape[0] / strides) - np.floor(patch_size / strides) + 1)

	return row_length, col_length


# convert row of values to square image
def row2square(X,row_length):
	n_rows = X.shape[0] / row_length
	X = np.split(X,n_rows,axis=0)
	X = np.expand_dims(X,0)
	return np.concatenate(X,axis=0)


# downsample images, return sorted sequence of image-name pairs
def get_sorted_seq(image_folder):
	return [[halfsize(cv2.imread(join(image_folder,p),0)),p] for p in sorted(os.listdir(image_folder))]


# save overlay of image and heatmap
def save_plot(X,im,save_name,idx,patch_size=90):
	X = cv2.resize(X,(im.shape[1]-int(patch_size/2),im.shape[0]-int(patch_size/2)))
	X = cv2.copyMakeBorder(X, 45, 45, 45, 45, cv2.BORDER_CONSTANT) # add padding to account for sliding window used to get hm so it lines up correctly with the image
	X = cv2.resize(X,(im.shape[1],im.shape[0])) # back to original size
	X = cv2.GaussianBlur(src = X, ksize = (15,15), sigmaX = 0) # smooth heatmap
	try:
		compare = np.uint8(0.6*X + 0.4*grey2rgb(im))
		final = np.uint8(np.concatenate([X,grey2rgb(im),compare],axis=1))
	except:
		compare = np.uint8(0.6*grey2rgb(X) + 0.4*grey2rgb(im))
		final = np.uint8(np.concatenate([grey2rgb(X),grey2rgb(im),compare],axis=1))
	plt.imshow(final)

	if not os.path.exists(save_name):
		os.mkdir(save_name)
	
	plt.savefig(join(save_name,str(idx+1)))


# all different visualisation methods used in save_all_plots
def pca_to_hsv_to_rgb(X):
	
	spher = spherical(X)
	for i in range(3):
		spher[:,i] = scale(spher[:,i],uint_scale=False)
	hsv_image = row2square(spher)
	
	return 255*mpcolors.hsv_to_rgb(hsv_image)

def centers_to_dist(lvec,km_model):
	c = km_model.cluster_centers_ 
	new_data = np.zeros((lvec.shape[0],c.shape[0]))
	for i in range(c.shape[0]):
		new_data[:,i] = np.linalg.norm(lvec - c[i,:],ord=2,axis=1)

	return new_data

def pca_to_heatmap(im,lvec,pca_20_3,save_name,idx):
	
	X = pca_20_3.transform(lvec)
	X = scale(row2square(X))

	save_plot(X,im,save_name,idx)

def kmeans_to_heatmap(im,lvec,km_model,save_name,idx):
	
	new_data = centers_to_dist(lvec,km_model)
	X = scale(row2square(new_data))

	save_plot(X,im,save_name,idx)

def kmeans_to_pca_to_heatmap(im,lvec,km_model,pca_model,save_name,idx):
	
	c = km_model.cluster_centers_ 
	new_data = np.zeros((lvec.shape[0],c.shape[0]))
	for i in range(c.shape[0]):
		new_data[:,i] = np.linalg.norm(lvec - c[i,:],ord=2,axis=1)
	new_data = pca_model.transform(new_data)
	
	X = scale(row2square(new_data))

	save_plot(X,im,save_name,idx)

def gmm_to_heatmap(im,lvec,gmm_model,save_name,idx):
	
	X = np.clip(np.float32(gmm_model.predict_proba(lvec)),0,1)
	X = scale(row2square(X))
	
	save_plot(X,im,save_name,idx)

def gmm_to_pca_to_heatmap(im,lvec,gmm_model,pca_model,save_name,idx):
	
	X = np.clip(np.float32(gmm_model.predict_proba(lvec)),0,1)
	X = scale(pca_model.transform(X))
	X = row2square(X)

	save_plot(X,im,save_name,idx)

def ave_dist_to_heatmap(im,lvec,save_name,idx):
	
	row_length, col_length = get_row_col_lengths(im,strides=20,patch_size=90)

	X = lvec / np.expand_dims(np.linalg.norm(lvec,2,axis=1),axis=1)
	
	diffs = []
	for i in range(X.shape[0]):
		tmp = X[i]*np.ones(X.shape)
		diffs.append(np.expand_dims((X[i,:] - tmp),axis=0))
	
	difference_matrix = np.concatenate(diffs,axis=0)
	distance_matrix = sklearn.metrics.pairwise.cosine_similarity(X)
	
	sims = []
	for sim in distance_matrix:
		sim = np.expand_dims(sim,axis=1)
		sim = row2square(sim,34)
		sims.append(sim)
	sims = np.dstack(sims)
	final_sim = grey2rgb(scale(np.mean(sims,axis=2)))
	
	save_plot(final_sim,im,save_name,idx)

def umap_to_heatmap(im, lvec, umap_model, save_name, idx):
	
	X = umap_model.transform(lvec)
	
	fig = pylab.figure()
	ax = fig.add_subplot(111,projection='3d')
	ax.scatter(X[:, 0], X[:, 1], X[:, 2], s=20, cmap='viridis', zorder=2)
	plt.close()
	
	X = scale(row2square(X))
	save_plot(X,im,save_name,idx)

def save_all_umap_plots(lvec,train_lvecs,idx):
	metric =  ['euclidean', 'l2', 'l1', 'manhattan', 'cityblock', 'braycurtis', 'canberra', 'chebyshev', 
				'correlation', 'cosine', 'mahalanobis', 'matching',
				'minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath', 'sqeuclidean', 
				'yule', 'wminkowski', 'haversine']
	
	for m in metric:
		reducer = umap.UMAP(n_neighbors=75, min_dist=0.0, n_components=2, n_epochs=150, metric='braycurtis')
		reducer.fit(train_lvecs)
		X = reducer.transform(train_lvecs)
		plt.figure(figsize = (10,8))
		ax = plt.gca()
		ax.axis('equal')
		ax.scatter(X[:, 0], X[:, 1], s=20, cmap='viridis', zorder=2)
		plt.savefig('{}'.format('test_all/{}-{}'.format(idx,m)))
		plt.close()


# save all dimensionality reduction plots
def save_all_plots(model, train_lvecs, image_folder, model_name, lsize=20, path='test_all'):
	save_path = join(path,model_name)
	if not os.path.exists(save_path):
		os.mkdir(save_path)	

	image_sequence = get_sorted_seq(image_folder)
	lsize = train_lvecs.shape[-1]
		
	_, pca_20_3 = get_PCA(train_lvecs,3)

	_, km_20_3 = get_kmeans(train_lvecs,3)
	
	_, km_20_6 = get_kmeans(train_lvecs,6)
	km_train = centers_to_dist(train_lvecs,km_20_6)
	_, pca_km_6_3 = get_PCA(km_train,3)

	_, gmm_20_3 = get_GMM(train_lvecs,3)

	gmm_train, gmm_20_6 = get_GMM(train_lvecs,6)
	_, pca_gmm_6_3 = get_PCA(gmm_train,3)

	reducer = umap.UMAP(n_neighbors=80, min_dist=0.0, n_components=10, n_epochs=100, metric='braycurtis')
	reducer.fit(train_lvecs)

	save_figs = True
	energies = []
	ims = []

	for idx,im in tqdm(enumerate(image_sequence)):
		im=im[0]
		lvec,_ = im_to_lvec(im,model,lsize,oneshot=True)
		if save_figs:
			pca_to_heatmap(im,lvec,pca_20_3,join(save_path,'pca2hm'),idx)
			kmeans_to_heatmap(im,lvec,km_20_3,join(save_path,'km2hm'),idx)
			kmeans_to_pca_to_heatmap(im,lvec,km_20_6,pca_km_6_3,join(save_path,'km2pca2hm'),idx)
			ave_dist_to_heatmap(im,lvec,join(save_path,'ad2hm'),idx)
			gmm_to_heatmap(im,lvec,gmm_20_3,join(save_path,'gmm2hm'),idx)
			gmm_to_pca_to_heatmap(im,lvec,gmm_20_6,pca_gmm_6_3,join(save_path,'gmm2pca2hm'),idx)
			umap_to_heatmap(im,lvec,reducer,join(save_path,'umap2hm'),idx)
		
# experimental alternate way of getting the heatmap, results very noisy and not that useful but left it in anyway, no need to use really
def get_pathway(model, image_sequence, model_name, lsize):
	
	# get various points in latent space going form dirt->dirt+food->food
	path_patches = [(51,512,1200),(3,215,800),(3,504,1243),(3,600,852),(92,256,760)]

	markers = []
	pathway = []
	# get patch lvecs
	for v in path_patches:
		patch = image_sequence[v[0]][0][int(v[1]-45):int(v[1]+45),int(v[2]-45):int(v[2]+45)]
		patch = prepro(patch,oneshot=False)
		lvec,_ = patch_to_lvec(patch,model,lsize,oneshot=False)
		markers.append(lvec)

	# interpolate between each lvec in latent space to get a 'latent pathway' from dirt to food
	for i in range(len(path_patches)-1):
		for j in list(np.linspace(0.0,1.0,25)[:-1]):
			zint = j * markers[i] + (1-j) * markers[i+1]
			pathway.append(zint)
		pathway.append(markers[i+1])
	pathway = np.concatenate(pathway,0)

	return pathway

# get distance from lvec to closest point on latent pathway
def get_score(lvec, pathway):
	# for each lvec, find closest point on latent pathway, the further along the path, the higher the value of argmin - brighter the pixel value.
	distance_matrix = sklearn.metrics.pairwise.euclidean_distances(lvec,pathway)
	return np.argmin(distance_matrix,axis=1)


# image equalisation based on inbtensity curve, parametrised by a,b,bump,off
# not used at them moment but can be quite useful, easiest way to see what it's doing is
# plugging in eqns at desmos.com/calculator and comparing to y=x in range 0-1.
def eq(x,a,b,bump,off=1):
	if bump:
		return (off*((1-(1-x)**a)**(1/b)) + (1-off))
	else:
		return x*(off*((1-(1-x)**a)**(1/b)) + (1-off))


# save reference latent vectors - CHANGE REF_PATCHES LIST WHEN SAVING FOR A NEW TESTBOX
def save_ref_vecs(model, image_sequence, model_name, lsize, oneshot):
	
	# first item is food, second is dirt
	# for each values are image_idx,row_start,height,col_start,width
	ref_patches_tb4 = [(1,432,303,573,753),(0,318,342,600,393)]
	ref_patches_tb5 = [(0,474,288,552,624),(1,426,369,543,666)]
	ref_patches_tb6 = [(1,453,324,552,381),(0,266,540,1104,280)]
	_,sh = im_to_lvec(image_sequence[0][0],model,lsize,oneshot=oneshot)
	
	all_vecs = []
	# get patches across the above rectangular areas in the image, and calculate an average across all their latent vectors
	for v in ref_patches_tb5:
		im = image_sequence[v[0]][0][v[1]:v[1]+v[2],v[3]:v[3]+v[4]]
		lvecs,_ = im_to_lvec(im,model,lsize,oneshot=False)
		ave_lvec = np.mean(lvecs,axis=0)
		ave_lvec = np.expand_dims(np.squeeze(ave_lvec),axis=0)
		
		if oneshot:
			ave_lvec = np.ones((sh[1]*sh[2],lsize)) * ave_lvec
		else:
			ave_lvec = np.ones((sh[0],lsize)) * ave_lvec
		all_vecs.append(ave_lvec)
	
	np.savez('../saves/ref_vecs_{}.npz'.format(lsize), lvecs=all_vecs)
	

# get heatmap values by finding distance between test image latent vecs and reference latent vecs
def compare_descriptors(lvec,fvec,dvec,oneshot):

	def get_dist(x,y,method='cos'):
		if method == 'euc':
			distance_matrix = sklearn.metrics.pairwise.euclidean_distances(x,y,squared=True)
		elif method == 'cos':
			distance_matrix = sklearn.metrics.pairwise.cosine_distances(x,y)
		return distance_matrix.diagonal()

	v1c = get_dist(lvec,dvec,'cos')
	v2c = get_dist(lvec,fvec,'cos')

	v1e = get_dist(lvec,dvec,'euc')
	v2e = get_dist(lvec,fvec,'euc')

	# how close is lvec to food lvec compared to dirt lvec:
	# 0 means v1c = 0, v2c > 0 so patch is dirt
	# 1 means v2c = 0 so patch is food
	v5c = v1c/(v2c+v1c)
	v5e = v1e/(v2e+v1e)
	
	return v5c


# write video of heatmaps, images and food from image folder
def plot_heatmaps(model, image_folder, model_name, tb, lsize, out_size, oneshot, strides=20, path='../heatmaps'):

	video_name = '../food_analysis_{}.avi'.format(model_name)
	patch_size = 90

	save_path = join(path,model_name)
	if os.path.exists(save_path):
		shutil.rmtree(save_path)
	os.mkdir(save_path)	

	image_sequence = get_sorted_seq(image_folder)
	
	save_ref_vecs(model, get_sorted_seq('../datasets/ref_ims/tb{}'.format(tb)), model_name, lsize, oneshot=oneshot)
	fvec,dvec = np.load('../saves/ref_vecs_{}.npz'.format(lsize))["lvecs"]
		
	prev_ims = []
	food = []
	time = []
	current = 0 # time in hours, every new image this number increases by 0.5
	max_food = 1e-4 # 1e-4 is placeholder, this number will be maximum food amount seen, used to estimate current percentage of food in tray
	
	for idx,pair in tqdm(enumerate(image_sequence)):
		# get heatmap from image
		im,name = pair
		lvec,sh = im_to_lvec(im,model,lsize,oneshot,strides)
		hm = compare_descriptors(lvec,fvec,dvec,oneshot)
		if oneshot:
			hm = grey2rgb(row2square(hm,sh[-2]))
		else:
			rl,_ = get_row_col_lengths(im,strides,patch_size)
			hm = grey2rgb(row2square(hm,rl))
			
		# if an entire image then no need to add padding to account for sliding window, else add padding
		if oneshot:
			hm = cv2.resize(hm,(im.shape[1],im.shape[0]),interpolation=cv2.INTER_NEAREST)
		else:
			hm = cv2.resize(hm,(im.shape[1]-int(patch_size/2),im.shape[0]-int(patch_size/2)),interpolation=cv2.INTER_NEAREST)
			hm = cv2.copyMakeBorder(hm, 45, 45, 45, 45, cv2.BORDER_CONSTANT)
			hm = cv2.resize(hm,(im.shape[1],im.shape[0]),interpolation=cv2.INTER_NEAREST)
		
		# increment time, get food from heatmap, add to lists
		food_amount = get_food_amount(hm,fac=5)
		food.append(food_amount)
		time.append(current)
		current += 0.5
		
		# update max_food 
		if food_amount > max_food:
			max_food = food_amount		
		
		# calculate percent of food, determine food amount based on thresholds
		t1 = 0.25
		t2 = 0.1

		percent_food = 100*food_amount / max_food
		if food_amount <= t1:
			if food_amount <= t2:
				message = 'no food left:feed'
			else:
				message = 'some food left:feed'
		else:
			message = 'lots of food left:dont feed'

		# rest of function is just making matplotlib figure
		name = name.split('.')[-2]
		colour_folder = '../datasets/testbox{}_colour'.format(tb)
		valid_im = [p for p in os.listdir(colour_folder) if name[:13] in p][0]
		impath = join(colour_folder,valid_im)
		colour_im = cv2.imread(impath)
		colour_im = cv2.cvtColor(colour_im,cv2.COLOR_BGR2RGB)
		
		plt.figure(figsize=(20,10))

		plt.subplot(2,2,1)
		plt.imshow(hm,cmap='gray',vmin=0.0,vmax=1.0)
		plt.title('{:.3f}: {:.1f}%'.format(food_amount, percent_food))
		plt.subplot(2,2,2)
		plt.imshow(im,cmap='gray')
		plt.title(name)
		plt.subplot(2,2,3)
		plt.ylim((0,0.8))
		try:
			plt.plot(time[-100:],food[-100:])
		except:
			plt.plot(time,food)
		plt.axhline(t1,color='r')
		plt.axhline(t2,color='g')
		plt.subplot(2,2,4)
		plt.imshow(0.6*scale((im),uint_scale=False)+0.4*hm[:,:,0],cmap='gray')
		plt.title(message)
		plt.axis('off')
		
		plt.savefig('{}/{}'.format(save_path,str(idx+1)))
		plt.close()

		# write frame to video file
		frame = cv2.imread('{}/{}.png'.format(save_path,idx+1))
		if idx == 0:
			h,w,c = frame.shape
			video = cv2.VideoWriter(video_name, 0, 2, (w,h))

		video.write(frame)

	video.release()


# convert image to row of lvecs for each patch
def im_to_lvec(im, model, lsize, oneshot, strides=20, patch_size=90):
	if oneshot:
		x = prepro(im,oneshot=oneshot)
		return patch_to_lvec(x,model,lsize,oneshot=True)
	else:
		patches = get_patches(im, patch_size=patch_size, strides=strides,aug=False)
		xs = prepro(patches,oneshot=oneshot)
		db = tf.data.Dataset.from_tensor_slices((xs))
		db = db.batch(64)
		lvecs=[]
		for step, x in enumerate(db):
			lat_vec_batch,_ = patch_to_lvec(x, model, lsize, oneshot=False, pool_size=4)
			lvecs.append(lat_vec_batch)
		lvecs = np.concatenate(lvecs,axis=0)
		
		return lvecs, lvecs.shape 


# needs work - determine food amount from image patch. Again best way to see what the function is doing is going to
# desmos.com/calculator and comparing below function with y=x
def get_food_amount(hm,fac):
	return np.mean(hm+(1.5*(hm**fac)))


