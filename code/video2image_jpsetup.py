import cv2
from os import listdir, path, mkdir
from os.path import isfile, join, expanduser
import sys
import numpy as np
import argparse


def get_file_list(file_path):

    file_list = [f for f in listdir(file_path) if isfile(join(file_path, f))]
    file_list.sort()

    return file_list


def process_video(cap, file_base_name, crop_frame, max_frames):

    counter = 0
    increment = 5
    done_frames = increment * max_frames
    while cap.isOpened() and counter < done_frames:
        if max_frames == 1:
            cap.set(1, cap.get(7)-1) 
        
        ret, frame = cap.read()

        if not ret:
            return
        if crop_frame:
            frame_crop = frame[:, 420:-420]

            half_width = int(frame_crop.shape[0] / 2)
            half_height = int(frame_crop.shape[1] / 2)

            quarter_frames = [frame_crop[:half_width, :half_height],
                              frame_crop[half_width:, :half_height],
                              frame_crop[:half_width, half_height:],
                              frame_crop[half_width:, half_height:]]

        else:
            frame_crop = frame
            quarter_frames = [frame]

        half_width = int(frame_crop.shape[0] / 2)
        half_height = int(frame_crop.shape[1] / 2)

        file_count_name = file_base_name + str(counter) + '_'
        print('writing for ', file_count_name)

        for frame in quarter_frames:

            save_name = file_count_name + '0.png'
            cv2.imwrite(save_name, frame)
            print('saved as ',save_name)
            if crop_frame:
                for angle in [90, 180, 270]:
                    center = tuple(np.array([half_height, half_width]) / 2)
                    rot_mat = cv2.getRotationMatrix2D(center, angle, 1.0)
                    new_image = cv2.warpAffine(
                        frame, rot_mat, (half_height, half_width))
                    save_name = file_count_name + str(angle) + '.png'
                    cv2.imwrite(save_name, new_image)

        counter += increment


def run(path_adjective="", crop_frame=True, max_frames=100000, file_path='size_classes/class4'):

    file_paths = [file_path]
    for file_path in file_paths:
        files = get_file_list(file_path)
        save_dir = '../datasets'

        for file in files:
            try:
                save_path = join(path_adjective,file[:-4])
                cap = cv2.VideoCapture(join(file_path, file))
                process_video(cap, save_path, crop_frame, max_frames)
            except:
                print('not a video format')


# Example: --save_path ../datasets/testbox6 --file_path ../datasets/recordings
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--save_path', help="Add string for path to save files",
                         type=str, required=True)
    parser.add_argument('--file_path', help="File path to source ",
                         type=str,required=True)
    parser.add_argument('--crop_frame', help="Crop frame ", type=bool, 
                        default = False, required=False)
    parser.add_argument('--max_frames', help="Maximum number of frames per clip ",
                         type=int, default= 1, required=False)

    args = parser.parse_args()
    print('crop frm: ', args.crop_frame)
    run(args.save_path, args.crop_frame, args.max_frames, args.file_path)
