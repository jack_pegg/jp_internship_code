import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import Sequential
from layers import ConvReluBN, MaxPool, Conv2D, UpSample, Dense, Dense2Conv, UpscaleRelu


# NOTE: A large number of these Encoder/Deocder/Models have not been used for a long time and may contain minor bugs, but have left them in anyway
# incase anyone wants to go back and experiment more with network architecture


class Encoder_Conv_1x1(keras.Model):

	def __init__(self, latent_size=3, act=keras.activations.linear):
		super(Encoder_Conv_1x1,self).__init__()
		self.act = act
		self.conv1 = ConvReluBN(64,5,2) 
		self.conv2 = ConvReluBN(128,3,2) 
		self.conv3 = ConvReluBN(256,3,2) 

		self.conv4 = ConvReluBN(128,3,2) 
		self.conv5 = ConvReluBN(64,3,2)
		self.conv6 = Conv2D(latent_size,1,1)

		self.out = keras.layers.AveragePooling2D(pool_size=4)

	def call(self, x, training=None):
		conv1 = self.conv1(x, training=training)
		conv2 = self.conv2(conv1, training=training)
		conv3 = self.conv3(conv2, training=training)
		conv4 = self.conv4(conv3, training=training)
		conv5 = self.conv5(conv4, training=training)
		conv6 = self.conv6(conv5)
		
		output = self.act(self.out(conv6))

		return output


class VAE_Encoder_Conv_1x1(keras.Model):

	def __init__(self, latent_size=3, reg=0.001):
		super(VAE_Encoder_Conv_1x1,self).__init__()

		self.conv1 = ConvReluBN(64,5,2,reg=reg) 
		self.conv2 = ConvReluBN(64,3,2,reg=reg) 
		self.conv3 = ConvReluBN(128,3,2,reg=reg) 
		self.conv4 = ConvReluBN(128,3,2,reg=reg)
		
		self.conv5_1 = Conv2D(latent_size,4,1,padding='valid',reg=reg)
		self.conv5_2 = Conv2D(latent_size,4,1,padding='valid',reg=reg)

	def call(self, x, training=None):
		conv1 = self.conv1(x, training=training)
		conv2 = self.conv2(conv1, training=training)
		conv3 = self.conv3(conv2, training=training)
		conv4 = self.conv4(conv3, training=training)
		
		mean = self.conv5_1(conv4)
		var = self.conv5_2(conv4)

		return mean, var


class Encoder_Conv_4x4(keras.Model):

	def __init__(self, latent_size=3, act=keras.activations.linear, reg=0.001):
		super(Encoder_Conv_4x4,self).__init__()
		self.act = act
		self.conv1 = ConvReluBN(64,5,1,reg=reg) 
		self.conv2 = ConvReluBN(64,3,2,reg=reg) 
		self.conv3 = ConvReluBN(128,3,2,reg=reg) 
		self.conv4 = ConvReluBN(128,3,2,reg=reg) 
		self.conv5 = ConvReluBN(256,3,2,reg=reg)
	
		self.out = Conv2D(latent_size,3,1,reg=reg) 

	def call(self, x, training=None):
		conv1 = self.conv1(x, training=training)
		conv2 = self.conv2(conv1, training=training)
		conv3 = self.conv3(conv2, training=training)
		conv4 = self.conv4(conv3, training=training)
		conv5 = self.conv5(conv4, training=training)

		output = self.act(self.out(conv5))
		
		return output


class VAE_Encoder_Conv_4x4(keras.Model):

	def __init__(self, latent_size=3, act=keras.activations.linear, reg=0.001):
		super(VAE_Encoder_Conv_4x4,self).__init__()
		self.act = act
		self.conv1 = ConvReluBN(64,5,1,reg=reg) 
		self.conv2 = ConvReluBN(64,3,2,reg=reg) 
		self.conv3 = ConvReluBN(128,3,2,reg=reg) 
		self.conv4 = ConvReluBN(128,3,2,reg=reg) 
		self.conv5 = ConvReluBN(256,3,2,reg=reg)
	
		self.conv6_1 = Conv2D(latent_size,3,1,reg=reg) # 64,96,lsize
		self.conv6_2 = Conv2D(latent_size,3,1,reg=reg) # 64,96,lsize

	def call(self, x, training=None):
		conv1 = self.conv1(x, training=training)
		conv2 = self.conv2(conv1, training=training)
		conv3 = self.conv3(conv2, training=training)
		conv4 = self.conv4(conv3, training=training)
		conv5 = self.conv5(conv4, training=training)

		mean = self.conv6_1(conv5)
		logvar = self.conv6_2(conv5)
		
		return mean, logvar


class Encoder_Conv_8x8(keras.Model):

	def __init__(self, latent_size=3, act=keras.activations.linear):
		super(Encoder_Conv_8x8,self).__init__()
		self.act = act
		self.conv1 = ConvReluBN(64,5,1) 
		self.conv2 = ConvReluBN(128,3,2) 
		self.conv3 = ConvReluBN(256,3,1) 

		self.conv4 = ConvReluBN(128,3,2) 
		self.conv5 = ConvReluBN(64,3,1) 
		self.conv6 = ConvReluBN(32,3,2) 
		
		self.out = Conv2D(latent_size,3,1) 

	def call(self, x, training=None):
		conv1 = self.conv1(x, training=training)
		conv2 = self.conv2(conv1, training=training)
		conv3 = self.conv3(conv2, training=training)
		conv4 = self.conv4(conv3, training=training)
		conv5 = self.conv5(conv4, training=training)
		conv6 = self.conv6(conv5, training=training)
		
		output = self.act(self.out(conv6))

		return output


class Decoder_Upscale(keras.Model):

	def __init__(self, singledim=None, act=keras.activations.relu):
		super(Decoder_Upscale,self).__init__()
		self.act = act
		self.singledim = singledim
		self.inp = Dense2Conv()
		self.decode1 = UpscaleRelu(64)
		self.decode2 = UpscaleRelu(128)
		self.decode3 = UpscaleRelu(128)
		self.decode4 = UpscaleRelu(64)
		self.decode5 = Conv2D(1,3,1)

	def call(self, x, training=None):
		if self.singledim == True:
			input_ = self.inp(x)
		else:
			input_ = x
		
		decode1 = self.decode1(input_)
		decode2 = self.decode2(decode1)
		decode3 = self.decode3(decode2)
		decode4 = self.decode4(decode3)
		
		output = self.act(self.decode5(decode4))

		return output


class Decoder_Upsample(keras.Model):

	def __init__(self, out_shape=[4,4,32], singledim=None, out_size=1, act=keras.activations.sigmoid, reg=0.001):
		super(Decoder_Upsample,self).__init__()
		self.slope = 0.0
		self.out_size = out_size
		self.act = act
		self.singledim = singledim
		if singledim == True:
			self.inp = Dense2Conv(out_shape=out_shape, slope=self.slope)
		self.decode1 = ConvReluBN(256,3,1,slope=self.slope,reg=reg)
		self.decode2 = ConvReluBN(128,3,1,slope=self.slope,reg=reg) 
		self.decode3 = ConvReluBN(128,3,1,slope=self.slope,reg=reg) 
		self.decode4 = ConvReluBN(64,3,1,slope=self.slope,reg=reg)		
		self.decode5 = ConvReluBN(32,5,1,slope=self.slope,reg=reg)
		self.decode6 = Conv2D(self.out_size,5,1,reg=reg)

	def call(self, x, input_size, training=None):
		if self.singledim == True:
			input_ = self.inp(x)
		else:
			input_ = x

		h,w = input_.shape[1], input_.shape[2]
		decode1 = self.decode1(input_, training=training)
		upsample1 = UpSample((2*h,2*w))(decode1) 
		decode2 = self.decode2(upsample1, training=training) 
		upsample2 = UpSample((4*h,4*w))(decode2)
		decode3 = self.decode3(upsample2, training=training) 
		upsample3 = UpSample((8*h,8*w))(decode3) 
		decode4 = self.decode4(upsample3, training=training) 
		upsample4 = UpSample((input_size[1],input_size[2]))(decode4)
		decode5 = self.decode5(upsample4, training=training)
		
		output = self.act(self.decode6(decode5))

		return output


# ==============================================================================


class AutoEncoder(keras.Model):

	def __init__(self, latent_size=3, act=keras.activations.sigmoid, slope=0.2):
		super(AutoEncoder,self).__init__()
		
		self.act = act
		self.slope = slope
		
		self.conv1 = ConvReluBN(64,5,1,padding='valid',slope=self.slope) # 60
		self.pool1 = MaxPool() # 30
		
		self.conv2_1 = ConvReluBN(128,5,1,padding='valid',slope=self.slope) # 26 
		self.conv2_2 = ConvReluBN(128,3,1,padding='valid',slope=self.slope) # 24
		self.pool2 = MaxPool() # 12
		
		self.conv3_1 = ConvReluBN(256,3,1,padding='valid',slope=self.slope) # 10 
		self.conv3_2 = ConvReluBN(256,3,1,padding='valid',slope=self.slope) # 8
		self.pool3 = MaxPool() # 4

		self.conv4 = Conv2D(latent_size,4,1,padding='valid') # 1

		self.dense = DenseReluBN(4*4*latent_size)
		self.reshape = keras.layers.Reshape([-1,4,4,latent_size])

		self.deconv1_1 = ConvReluBN(256,3,1,padding='same',slope=self.slope)
		self.deconv1_2 = ConvReluBN(256,3,1,padding='same',slope=self.slope)

		self.deconv2 = ConvReluBN(128,3,1,padding='same',slope=self.slope)

		self.deconv3 = ConvReluBN(128,3,1,padding='same',slope=self.slope)

		self.deconv4 = ConvReluBN(64,3,1,padding='same',slope=self.slope)
		
		self.out = Conv2D(1,5,1,padding='same')


	def call(self, x, training=None):
		
		conv1 = self.conv1(x, training=training)
		pool1 = self.pool1(conv1)
		
		conv2_1 = self.conv2_1(pool1, training=training)
		conv2_2 = self.conv2_2(conv2_1, training=training)
		pool2 = self.pool2(conv2_2)
		
		conv3_1 = self.conv3_1(pool2,training=training)
		conv3_2 = self.conv3_2(conv3_1, training=training)
		pool3 = self.pool3(conv3_2)

		conv4 = self.conv4(pool3, training=training)

		dense = self.dense(conv4.Flatten())
		reshape = self.reshape(dense)

		size = reshape.shape[1] # 4
		
		deconv1_1 = self.deconv1_1(reshape, training=training)
		deconv1_2 = self.deconv1_2(deconv1_1, training=training)
		ups1 = UpSample((size*2,size*2))(deconv1_2)

		deconv2 = self.deconv2(ups1, training=training)
		ups2 = UpSample((size*4,size*4))(deconv2)

		deconv3 = self.deconv3(ups2, training=training)
		ups3 = UpSample((size*8,size*8))(deconv3)

		deconv4 = self.deconv4(ups3, training=training)
		ups4 = UpSample((size*8,size*8))(deconv4)

		output = self.act(self.out(ups4))

		return conv4, output


class DenseAutoEncoder(keras.Model):

	def __init__(self, latent_size=3, act = keras.activations.sigmoid):
		super(DenseAutoEncoder,self).__init__()
		self.act = act
		self.lat = Sequential([ConvReluBN(64,5,1),
									ConvReluBN(128,3,1),
									MaxPool(),
									ConvReluBN(128,3,1),
									ConvReluBN(128,3,1),
									MaxPool(),
									ConvReluBN(128,3,1),
									keras.layers.Flatten(),
									Dense(128),
									keras.layers.ReLU(),
									keras.layers.Dropout(0.2),
									Dense(latent_size),
									keras.layers.ReLU()])
		
		self.rec = Sequential([Dense(128),
									keras.layers.ReLU(),
									Dense(16*16*64),
									keras.layers.ReLU(),
									keras.layers.Reshape([16,16,64]),
									DeconvReluBN(128,3,2),
									ConvReluBN(128,3,1),
									DeconvReluBN(128,3,2),
									ConvReluBN(64,5,1),
									Conv2D(1,3,1)])

	def call(self, x, training=None):
		lat = self.lat(x, training=training)
		out = self.rec(lat, training=training)
		
		return lat, self.act(out)


class AE_4x4(keras.Model):

	def __init__(self, latent_size=3, out_size=1, reg=0.001,act=keras.activations.sigmoid):
		super(AE_4x4,self).__init__()
		
		self.encoder = Encoder_Conv_4x4(latent_size=latent_size, reg=reg)
		self.decoder = Decoder_Upsample(singledim=False, out_size=out_size, reg=reg, act=act)

	def call(self, x, training=None):
		x = tf.keras.layers.GaussianNoise(.02)(x, training=training)
		latent_vector = self.encoder(x, training=training)
		reconstructed = self.decoder(latent_vector, x.shape, training=training)

		return latent_vector, reconstructed


class VAE_1x1(keras.Model):
	
	def __init__(self, latent_size=3, out_size=1, reg=0.001, act=keras.activations.sigmoid):
		super(VAE_1x1,self).__init__()
		self.encoder = VAE_Encoder_Conv_1x1(latent_size=latent_size, reg=reg)
		self.decoder = Decoder_Upsample(singledim=True, out_shape=[4,4,latent_size], out_size=out_size, reg=reg, act=act)

	def sample(self,mean,log_var):
		std = tf.exp(0.5 * log_var)
		eps = tf.random.normal(tf.shape(std))

		return mean + eps * std

	def interpolate(self,imgs):
		slides = []
		[_,_,z],_ = self.call(imgs, training=False)
		for i in range(len(imgs)-1):
			z1 = z[i]
			z2 = z[i+1]
			patches = []
			for j in list(np.linspace(0.0,1.0,10)):
				zint = tf.expand_dims(j * z1 + (1-j) * z2,0)
				imint = self.decoder(zint, (1,64,64,1), training=False).numpy()
				patches.append(imint)
			patches = np.concatenate(np.squeeze(patches),axis=1)
			slides.append(patches)
		slides = np.concatenate(slides,axis=0)

		return slides

	def call(self, x, training=None):
		x = tf.keras.layers.GaussianNoise(.02)(x, training=training)
		mean,log_var = self.encoder(x, training=training)
		z = self.sample(mean,log_var)
		reconstructed = self.decoder(z, x.shape, training=training)

		return [mean, log_var, z], reconstructed


class VAE_4x4(keras.Model):

	def __init__(self, latent_size=3, out_size=1, reg=0.001, act=keras.activations.sigmoid):
		super(VAE_4x4,self).__init__()
		self.latent_size = latent_size
		self.encoder = VAE_Encoder_Conv_4x4(latent_size=latent_size, reg=reg)
		self.decoder = Decoder_Upsample(singledim=False, out_size=out_size, reg=reg, act=act)

	def sample(self,mean,log_var):
		std = tf.exp(0.5 * log_var)
		eps = tf.random.normal(tf.shape(std))
		return mean + eps * std

	def show_latentspace(self, x):
		mean, log_var = self.encoder(x, training=False)
		mean, log_var = mean.numpy(), log_var.numpy()
		std = np.exp(0.5 * log_var)
		
		slides = []
		for i in range(self.latent_size):
			patches = []
			for j in list(np.linspace(-3.0,3.0,10)):
				eps = np.random.normal(0,1,log_var.shape)
				eps[...,i] = eps[...,i] + np.ones(eps[...,i].shape) * j
				z = tf.convert_to_tensor(eps, dtype=tf.float32)
				recon = self.decoder(z, x.shape, training=False)
				patches.append(np.squeeze(recon.numpy()))
			patches = np.concatenate(patches,axis=1)
			slides.append(patches)
		slides = np.concatenate(slides,axis=0)

		return slides

	def interpolate(self,imgs):
		slides = []
		[_,_,z],_ = self.call(imgs, training=False)
		for i in range(len(imgs)-1):
			z1 = z[i]
			z2 = z[i+1]
			patches = []
			for j in list(np.linspace(0.0,1.0,10)):
				zint = tf.expand_dims(j * z1 + (1-j) * z2,0)
				imint = self.decoder(zint, (1,64,64,1), training=False).numpy()
				patches.append(imint)
			patches = np.concatenate(np.squeeze(patches),axis=1)
			slides.append(patches)
		slides = np.concatenate(slides,axis=0)

		return slides


	def call(self, x, training=None):
		x = tf.keras.layers.GaussianNoise(.02)(x, training=training)
		mean, log_var = self.encoder(x, training=training)
		
		sh = mean.shape
		flat_mean = tf.reshape(mean,[-1,self.latent_size])
		flat_log_var = tf.reshape(log_var,[-1,self.latent_size])
		z = self.sample(flat_mean,flat_log_var)
		z = tf.reshape(z,[-1,sh[1],sh[2],self.latent_size])
		
		reconstructed = self.decoder(z, x.shape, training=training)

		return [mean, log_var, z], reconstructed


class Discriminator(keras.Model):

	def __init__(self):
		super(Discriminator,self).__init__()
		self.slope = 0.2
		self.disc1 = ConvReluBN(32,4,2,slope=self.slope)
		self.disc2 = ConvReluBN(64,4,2,slope=self.slope)
		self.disc3 = ConvReluBN(128,4,2,slope=self.slope)
		self.disc4 = ConvReluBN(256,4,2,slope=self.slope)
		self.out = Conv2D(1,4,1,padding='valid')

	def call(self, x, training=None):
		disc1 = self.disc1(x, training=training)
		disc2 = self.disc2(disc1, training=training)
		disc3 = self.disc3(disc2, training=training)
		disc4 = self.disc4(disc3, training=training)
		
		out = self.out(disc4)
		
		return out


class Generator(keras.Model):
	
	def __init__(self, latent_size=3):
		super(Generator,self).__init__()
		self.encoder = Encoder_Conv_8x8(latent_size=latent_size)
		self.decoder = Decoder_Upscale(act=keras.activations.tanh)

	def call(self, x, training=None):
		latent_vector = self.encoder(x, training=training)
		reconstructed = self.decoder(latent_vector, training=training)

		return latent_vector, reconstructed
