# for training multiple models sequentially
python3 train.py --mode train --latent_size 30 --model_name AE_4x4 --n_epochs 8 --batch_size 40 --reg 0.001 --testbox 6
python3 train.py --mode train --latent_size 32 --model_name AE_4x4 --n_epochs 8 --batch_size 40 --reg 0.005 --testbox 6
python3 train.py --mode train --latent_size 30 --model_name AE_4x4 --n_epochs 8 --batch_size 40 --reg 0.001 --testbox 5
python3 train.py --mode train --latent_size 20 --model_name AE_4x4 --n_epochs 8 --batch_size 40 --reg 0.001 --testbox 5
python3 train.py --mode train --latent_size 40 --model_name VAE_4x4 --n_epochs 20 --batch_size 40 --reg 0.0001 --testbox 5
python3 train.py --mode train --latent_size 20 --model_name VAE_4x4 --n_epochs 20 --batch_size 40 --reg 0.0001 --testbox 5
python3 train.py --mode train --latent_size 64 --model_name VAE_1x1 --n_epochs 20 --batch_size 40 --reg 0.0001 --testbox 5
