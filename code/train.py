import tensorflow as tf
from tensorflow import keras
import numpy as np
import sklearn
from datetime import datetime
import argparse
import os

import utils
import models
import layers
#from tensorflow.contrib.tensorboard.plugins import projector

parser = argparse.ArgumentParser()

parser.add_argument('--model_name', help="Model to import ",
					 type=str,default='VAE_4x4')
parser.add_argument('--mode', help="choose from [train,heatmaps] ",
					 type=str)

parser.add_argument('--testbox', help="which dataset is being used",
					 type=int, default=6)
parser.add_argument('--latent_size', help="latent_size ",
					 type=int,default=20)
parser.add_argument('--batch_size', help="size of batches ",
					 type=int,default=32)
parser.add_argument('--n_epochs', help="number of epochs ",
					 type=int,default=50)
parser.add_argument('--out_size', help="number of output channels ",
					 type=int,default=1)
parser.add_argument('--reshape_size', help="number of output channels ",
					 type=int,default=64)

parser.add_argument('--reg', help="weight regularisation coeff ",
					 type=float,default=0.002)
parser.add_argument('--beta', help="BVAE hyperparameter ",
					 type=float,default=1e-3)

parser.add_argument('--oneshot', help="input whole image at once? ",
					type=int,default=0)
parser.add_argument('--strides', help="strides to use in heatmap function ",
					type=int,default=20)

args = parser.parse_args()

# import correct model
if args.model_name == 'AE_4x4':
	from models import AE_4x4 as tf_model

if args.model_name == 'VAE_4x4':
	from models import VAE_4x4 as tf_model

if args.model_name == 'VAE_1x1':
	from models import VAE_1x1 as tf_model


# setup names based on command line arguments
col = 1 if args.out_size == 3 else 0
load_name = '../datasets/train_data_tb{}_c{}_os{}.npz'.format(args.testbox,col,args.oneshot)
folder_name = '{}_{}_{}_{}'.format(args.model_name,args.latent_size,args.out_size,args.oneshot)



def prepro_batch(x,size=None):
	
	# make 4dimensional
	if len(x.shape) == 3:
		x = tf.expand_dims(x,-1)
	if len(x.shape) == 2:
		x = tf.expand_dims(x,-1)
		x = tf.expand_dims(x, 0)
	
	if size is not None:
		x = utils.resize_tensor(x,size)
	else:
		# slightly downsample, patch size 90->64 is same fraction as 1024,1536->728,1092
		if args.oneshot:
			x = utils.resize_tensor(x,(728,1092))
		else:
			x = utils.resize_tensor(x,(args.reshape_size,args.reshape_size))
	
	if args.out_size == 3:
		# used for autoencoder going from grey to colour images to exaggerate difference in colour between maggots, dirt etc.
		x = tf.image.adjust_contrast(x,1.5)
	
	return x / 255. 


# get lvec from a single patch
def get_feature_vec(model, image):
	try:
		lat,_ = model(image, training=False)
		size = int(lat.numpy().shape[1])
	except:
		[lat,_,_],_ = model(image, training=False)
		size = int(lat.numpy().shape[1])
	
	if size != 1:
		# pool from size,size,lsize -> 1,1,lsize then flatten for lvec
		lat = keras.layers.AveragePooling2D(pool_size=size)(lat)
		lat = keras.layers.Flatten()(lat)

	return lat.numpy()


# get (roughly) n_samples worth of latent vectors, used when fitting pca/gmm/etc. models, not used in training
def load_lvecs(db, model, n_samples=5000):
	lvecs = []
	xs = []
	batch_size = 20
	for step, x in enumerate(db):
		x = prepro_batch(x)
		lat_vec_batch = get_feature_vec(model, x)
		lvecs.append(lat_vec_batch)
		xs.append(x)
		if step > n_samples / batch_size:
			break
	all_vecs = np.concatenate(lvecs,axis=0)
	all_xs = np.concatenate(xs,axis=0)

	return all_vecs, all_xs


def main():

	lr = 0.0005
	# load model and dataset
	db, n_batches = utils.get_BSF_dataset(args.batch_size,load_name)
	model = tf_model(args.latent_size, args.out_size, args.reg, act=keras.activations.sigmoid)
	
	if args.oneshot:
		model.build(input_shape=(None,728,1092,1))
	else:
		model.build(input_shape=(None,args.reshape_size,args.reshape_size,1))
	
	model.summary()

	# setup log directory and save/log folders
	logdir = "../logs/{}/".format(folder_name) + datetime.now().strftime("%d/%m-%H:%M:%S")
	file_writer = tf.summary.create_file_writer(logdir)

	path = os.path.join('../saves',folder_name)
	if not os.path.exists(path):
		os.mkdir(path)

	path = os.path.join('../logs',folder_name)
	if not os.path.exists(path):
		os.mkdir(path)


	for epoch in range(args.n_epochs):
		# update optimiser with new learning rate (changes at end of each epoch) and shuffle dataset
		opt = keras.optimizers.Adam(lr=lr)
		db = db.shuffle(2048)

		# try except used to be able to save model after ctrl C
		try:
			for step, x in enumerate(db):
				if args.out_size == 1:
					x = prepro_batch(x)
					y = x
				elif args.out_size == 3:
					y = prepro_batch(x)
					x = tf.image.rgb_to_grayscale(y)
				
				with tf.GradientTape() as tape:
					# get loss value depending on model used
					if 'VAE' in args.model_name:
						args.beta = np.min([1,((epoch*n_batches)+step)/10000])
						latentvec, prediction = model(x, training=True)
						mean, logvar, z = latentvec
						recon_loss = -tf.reduce_sum(y * tf.math.log(1e-10 + prediction) + (1 - y) * tf.math.log(1e-10 + 1 - prediction), axis=[1,2,3])
						#recon_loss = 0.5 * tf.reduce_sum(1 - tf.compat.v1.image.ssim(y,prediction,max_val=1,filter_size=6,filter_sigma=1.5)) * (args.reshape_size*args.reshape_size)
						#recon_loss = tf.reduce_sum(tf.square(y - prediction), axis=[1,2,3])
						kl_div = - 0.5 * tf.reduce_sum(1. + logvar - tf.square(mean) - tf.exp(logvar), axis=[1,2,3])
						kl_loss = args.beta * kl_div
						loss = tf.reduce_mean(recon_loss + kl_loss)
					else:
						latentvec, prediction = model(x, training=True)
						loss = 0.5 * tf.reduce_mean(1 - tf.compat.v1.image.ssim(y,prediction,max_val=1,filter_size=6,filter_sigma=1.5))
				
				# apply gradients
				grads = tape.gradient(loss, model.trainable_variables)
				opt.apply_gradients(zip(grads, model.trainable_variables))

				# print loss, update logs
				if step % 20 == 0:
					print('{}/{}'.format(epoch+1,args.n_epochs), '{}/{}'.format(step, int(n_batches)), 'loss:', float(loss))
					with file_writer.as_default():
						try:
							tf.summary.scalar("loss", loss, step=(epoch*n_batches)+step)
							tf.summary.scalar("recon_loss", tf.reduce_mean(recon_loss), step=(epoch*n_batches)+step)
							tf.summary.scalar("kl_loss", tf.reduce_mean(kl_loss), step=(epoch*n_batches)+step)
							tf.summary.scalar("beta", args.beta, step=(epoch*n_batches)+step)
						except:
							tf.summary.scalar("loss", loss, step=(epoch*n_batches)+step)

				# update image logs
				if step % int(n_batches/5) == 0:
					_, prediction = model(x, training=False)
					
					if args.out_size == 1:
						tb_predicted = utils.grey2rgb(prediction[:16])
						tb_original = utils.grey2rgb(y[:16])
					else:
						tb_predicted = prediction[:16]
						tb_original = y[:16]
					
					tb_compare = [np.concatenate((o,p),axis=1) for o,p in zip(tb_original,tb_predicted)]

					with file_writer.as_default():
						tf.summary.image("image_comparison", utils.scale(tb_compare), max_outputs=16, step=(epoch*n_batches)+step)

					if 'VAE' in args.model_name:
												
						slides = model.interpolate(x[:20])
						slides = np.expand_dims(slides,-1)
						slides = np.expand_dims(slides,0)

						with file_writer.as_default():
							tf.summary.image("view_latentspace", utils.scale(slides), max_outputs=16, step=(epoch*n_batches)+step)
							[tf.summary.histogram(w.name + '_weight',w.get_weights()[0],step=(epoch*n_batches)+step) for w in model.layers[0].layers[-2:]]
							[tf.summary.histogram(w.name + '_bias',w.get_weights()[1],step=(epoch*n_batches)+step) for w in model.layers[0].layers[-2:]]
			
		
		except KeyboardInterrupt:
			print('saving...')
			model.save_weights("../saves/{}/AE_interrupt.h5".format(folder_name))
			print('saved!')
			return
		model.save_weights("../saves/{}/AE_checkpoint.h5".format(folder_name))
		
		lr *= 0.95
	
	model.save_weights("../saves/{}/AE_finished.h5".format(folder_name))


# save video analysis file with images, heatmap and graph
def get_heatmaps():
	model = tf_model(args.latent_size, args.out_size)
	if args.oneshot:
		model.build(input_shape=(None,728,1092,1))
	else:
		model.build(input_shape=(None,args.reshape_size,args.reshape_size,1))
	try:
		path = '../saves/{}/AE_checkpoint.h5'.format(folder_name)
		model.load_weights(path)
	except:
		path = '../saves/YES_AE_10_tb6/AE_checkpoint.h5'
		model.load_weights(path)
	
	image_folder = '../datasets/testbox{}'.format(args.testbox)
	
	utils.plot_heatmaps(model,image_folder,folder_name,args.testbox,args.latent_size,args.out_size,args.oneshot,args.strides)


if __name__ == '__main__':
	if args.mode == 'train':
		main()
	if args.mode == 'heatmaps':
		get_heatmaps()