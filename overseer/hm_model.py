import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import Sequential
from tensorflow.keras.regularizers import l2


def Conv2D(ch, ksize, strides, padding='same', bias=True, init='glorot_uniform',reg=0.001):
	return keras.layers.Conv2D(ch, ksize, strides, padding=padding, use_bias=bias, kernel_initializer=init, kernel_regularizer=l2(reg))

def UpSample(output_size):
	return keras.layers.Lambda(lambda x: tf.compat.v1.image.resize(x, output_size, align_corners=True))

def Deconv(ch, ksize, strides, bias=True):
	return keras.layers.Conv2DTranspose(ch, ksize, strides, padding='same', use_bias=bias)

def Dense(units, use_bias=True, reg=0.001):
	return keras.layers.Dense(units, use_bias=use_bias, kernel_regularizer=l2(reg))

def MaxPool(size=(2,2), strides=2, padding='valid'):
	return keras.layers.MaxPool2D(pool_size=size,strides=strides,padding=padding)

def AvePool(size=(2,2), strides=2, padding='valid'):
	return keras.layers.AveragePooling2D(pool_size=size,strides=strides,padding=padding)


class ConvReluBN(keras.layers.Layer):

	def __init__(self, ch, ksize, strides, padding='same', slope=0, reg=0.001):
		super(ConvReluBN,self).__init__()
		self.out = keras.Sequential([
			Conv2D(ch,ksize,strides,padding=padding,bias=False,reg=reg),
			keras.layers.ReLU(negative_slope=slope),
			keras.layers.BatchNormalization()
			])

	def call(self, x, training=None):
		return self.out(x, training=training)


class DenseReluBN(keras.layers.Layer):

	def __init__(self, units, slope=0, reg=0.001):
		super(DenseReluBN,self).__init__()
		self.out = keras.Sequential([
			Dense(units, use_bias=False, reg=reg),
			keras.layers.ReLU(negative_slope=slope),
			keras.layers.BatchNormalization()
			])

	def call(self, x, training=None):
		return self.out(x, training=training)


class DeconvReluBN(keras.layers.Layer):

	def __init__(self, ch, ksize, strides, slope=0):
		super(DeconvReluBN,self).__init__()
		self.out = keras.Sequential([
			Deconv(ch,ksize,strides,bias=False),
			keras.layers.ReLU(negative_slope=slope),
			keras.layers.BatchNormalization()
			])

	def call(self, x, training=None):
		return self.out(x, training=training)


class Dense2Conv(keras.layers.Layer):

	def __init__(self, out_shape=[8,8,32], slope=0):
		super(Dense2Conv,self).__init__()
		self.out_shape = out_shape
		self.decode1 = DenseReluBN(256,slope=slope)
		self.decode2 = DenseReluBN(out_shape[0]*out_shape[1]*out_shape[2],slope=slope)
		
	def call(self, x, training=None):
		decode1 = self.decode1(x, training=training)
		decode2 = self.decode2(decode1, training=training)

		return tf.reshape(decode2,[-1,self.out_shape[0], self.out_shape[1], self.out_shape[2]])



class Encoder_Conv_4x4(keras.Model):

	def __init__(self, large=True, latent_size=3, act=keras.activations.linear):
		super(Encoder_Conv_4x4,self).__init__()
		self.large=large
		self.act = act
		self.conv1 = ConvReluBN(64,5,1) 
		self.conv2 = ConvReluBN(64,3,2) 
		self.conv3 = ConvReluBN(128,3,2) 
		self.conv4 = ConvReluBN(128,3,2) 
		self.conv5 = ConvReluBN(256,3,2)
		if self.large == True:
			self.conv6 = ConvReluBN(256,3,2) 
		
		self.out = Conv2D(latent_size,3,1) 

	def call(self, x, training=None):
		conv1 = self.conv1(x, training=training)
		conv2 = self.conv2(conv1, training=training)
		conv3 = self.conv3(conv2, training=training)
		conv4 = self.conv4(conv3, training=training)
		conv5 = self.conv5(conv4, training=training)
		if self.large == True:
			conv6 = self.conv6(conv5, training=training)
		else:
			conv6 = conv5

		output = self.act(self.out(conv6))
		
		return output



class Decoder_Upsample(keras.Model):

	def __init__(self, out_shape=[8,8,32], large=True, singledim=None, out_size=1, act=keras.activations.sigmoid):
		super(Decoder_Upsample,self).__init__()
		self.large = large
		self.slope = 0.0
		self.out_size = out_size
		self.act = act
		self.singledim = singledim
		if singledim == True:
			self.inp = Dense2Conv(out_shape=out_shape, slope=self.slope)
		self.decode1 = ConvReluBN(256,3,1,slope=self.slope)
		self.decode2 = ConvReluBN(128,3,1,slope=self.slope) 
		self.decode3 = ConvReluBN(128,3,1,slope=self.slope) 
		self.decode4 = ConvReluBN(64,3,1,slope=self.slope)
		if self.large == True:
			self.decode5 = ConvReluBN(64,3,1,slope=self.slope) 
		
		self.decode6 = ConvReluBN(32,5,1,slope=self.slope)
		self.decode7 = Conv2D(self.out_size,5,1)

	def call(self, x, training=None):
		if self.singledim == True:
			input_ = self.inp(x)
		else:
			input_ = x

		size = input_.shape[1]
		decode1 = self.decode1(input_, training=training)
		upsample1 = UpSample((2*size,2*size))(decode1) 
		decode2 = self.decode2(upsample1, training=training) 
		upsample2 = UpSample((4*size,4*size))(decode2)
		decode3 = self.decode3(upsample2, training=training) 
		upsample3 = UpSample((8*size,8*size))(decode3) 
		decode4 = self.decode4(upsample3, training=training) 
		upsample4 = UpSample((16*size,16*size))(decode4)
		if self.large == True:
			decode5 = self.decode5(upsample4, training=training) 
			upsample5 = UpSample((32*size,32*size))(decode5)
		else:
			upsample5 = upsample4

		decode6 = self.decode6(upsample5, training=training)
		
		output = self.act(self.decode7(decode6))

		return output



class AE_4x4(keras.Model):

	def __init__(self, latent_size=3, out_size=1):
		super(AE_4x4,self).__init__()
		self.encoder = Encoder_Conv_4x4(large=False, latent_size=latent_size)
		self.decoder = Decoder_Upsample(large=False, singledim=False, out_size=out_size)

	def call(self, x, training=None):
		latent_vector = self.encoder(x, training=training)
		reconstructed = self.decoder(latent_vector, training=training)

		return latent_vector, reconstructed