#! /usr/bin/env python3

import rospy
from std_msgs.msg import Bool, Float32
from os.path import expanduser, isdir
from sys import path,argv
from util import get_title, ros_sleep
from os import makedirs
from feeding_box.srv import SendFrame

import time
import cv2
import argparse
import numpy as np
import tensorflow as tf



from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError 

segmentation_path = expanduser('~/scripts/heatmaps')
path.append(segmentation_path)

import hm_utils
from hm_model import AE_4x4 as tf_model
from data_creator import ImagePreparator


class FeedMeasurer:
    
    light_state = False
    light_time = -1
    min_wait_time_light_off = 16 #seconds

    classes = 1    
    image_size = -1
    
    def __init__(self, interval, latent_size, path_to_model, rv_path, save_folder, out_size=1):

        self.save_folder = expanduser(save_folder)
        if not isdir(self.save_folder):
            makedirs(self.save_folder)
        self.interval = interval
        self.cv_bridge = CvBridge()
        mean_data = 110
        std_data = 60
        scale = 0.4
        self.path_to_model = path_to_model
        self.latent_size = latent_size
        self.out_size = out_size
        self.rv_path = rv_path
        self.prev_frame = None

        self.loader = ImagePreparator(mean_data, std_data, scale)
        
        rospy.wait_for_service('light_serv')
        rospy.wait_for_service('camera_serv')
        rospy.Subscriber('light_state', Bool, self.listen_light)
        rospy.init_node('feed_measurer_pub')

        self.pub_overlay = rospy.Publisher('feed_segmented_overlay',Image,queue_size=10)
        self.pub = rospy.Publisher('feed_food_amount',Float32,queue_size=10)
        rospy.Subscriber('do_measure_frac', Bool, self.do_measurement)

        self.bridge = CvBridge()
        self.run_loop()


    def run_loop(self):
        tf.enable_eager_execution() 
        while not rospy.is_shutdown():
            self.prev_frame = self.do_measurement(True)
            ros_sleep(self.interval)
    

    def request_frame(self):
        frame_reader = rospy.ServiceProxy('camera_serv', SendFrame)
        try:
            resp = frame_reader()
            frame_reader.close()
            frame = self.bridge.imgmsg_to_cv2(resp.frame)
            return True, frame
        except:
            frame_reader.close()
            rospy.logerr("could not access camera_serv")
            return False, []


    def listen_light(self,data):
        self.light_state = data.data
        self.light_time = time.time()


    def wait_light_off(self):
        
        rate = rospy.Rate(0.2)
        while not rospy.is_shutdown():
            plenty_time_passed = self.min_wait_time_light_off < time.time() -self.light_time

            if not self.light_state and plenty_time_passed:
                return
            rate.sleep()
            if rospy.is_shutdown():
                exit(0)
    

    def show_overlay(self, heatmap, frame, food_amount):
        '''
        if len(frame.shape)<3:
            overlay_im = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        else:
            overlay_im = frame.copy()
        overlay_im = hm_utils.halfsize(overlay_im) # 1024,1536,3
        '''
        #h,w = overlay_im.shape[:2]
        #heatmap = cv2.resize(heatmap,(w,h))

        hm_title = get_title(self.save_folder,'hm_rslt.png')
        hm_org_title = get_title(self.save_folder,'hm_org.png')
        
        heatmap = (255*heatmap).astype(np.uint8)
        cv2.imwrite(hm_title,heatmap)

        rospy.loginfo('saved overlay in %s',hm_title )
        encoding = "bgr8"
        overlay_enc = self.cv_bridge.cv2_to_imgmsg(heatmap,encoding)
        self.pub_overlay.publish(overlay_enc)


    def do_measurement(self, input):

        self.wait_light_off()
        ret, frame = self.request_frame()
        if ret:
            frame = cv2.cvtColor(frame,cv2.COLOR_RGB2GRAY) # 2048,3072
            
            model = tf_model(self.latent_size,self.out_size)
            model.build(input_shape=(None,64,64,1))
            model.load_weights(self.path_to_model)

            ref_vecs = np.load(self.rv_path)["lvecs"] # self.latent_size
            heatmap, food_amount = hm_utils.get_heatmap(frame, self.prev_frame, model, ref_vecs) # 49,73
            self.show_overlay(heatmap,frame,food_amount)

            rospy.loginfo('Measured feed food_amount %f',food_amount)
            self.pub.publish(food_amount)
        else:
            self.pub.publish(-1)

        return frame
        
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--interval', help="period for measuring feed",
                        type=int, required=True)
    parser.add_argument('-l','--latent_size', help="size of latent vecs",
                        type=int, required=True)
    parser.add_argument('-p','--model_path', help="path to the model",
                        type=str, required=True)
    parser.add_argument('-r','--rv_path', help="path to the reference latent vectors",
                        type=str, required=True)
    parser.add_argument('-f','--save_path', help="folder where to store resulting overlay",
                        type=str, required=True)

    ros_argv = rospy.myargv(argv=argv)[1:]
    args = parser.parse_args(ros_argv)
    model_path = expanduser(args.model_path)
    rv_path = expanduser(args.rv_path)
    save_path = expanduser(args.save_path)
    measurer = FeedMeasurer(args.interval, args.latent_size, model_path, rv_path, save_path)