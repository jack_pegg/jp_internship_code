import numpy as np
import cv2
import sys
import os
import time
from os.path import join
import argparse
import sklearn

from PIL import Image
from PIL import ImageFilter
import shutil

from scipy import ndimage

import tensorflow as tf
from tensorflow import keras


# scale array to 0,1 float or 0,255 uint8, useful for viewing matplotlib plots
def scale(img,uint_scale=True):
	X = ((img - np.min(img)) / (np.max(img) - np.min(img)))
	if uint_scale:
		return np.uint8(255*X)
	else:
		return X


# cvt grayscale img to rgb
def grey2rgb(img):
	img = np.squeeze(img)
	return np.stack([img,img,img],axis=-1)


# resize image tensor
def resize_tensor(tensor, size):
	return tf.compat.v1.image.resize(tensor,size,align_corners=True)


# resize and normalise tensor
def prepro(x,oneshot,size=None,contrast=False):
	x = tf.convert_to_tensor(x, dtype=tf.float32) 
	# make 4dimensional
	if len(x.shape) == 3:
		x = tf.expand_dims(x,-1)
	if len(x.shape) == 2:
		x = tf.expand_dims(x,-1)
		x = tf.expand_dims(x, 0)		
	if size is not None:
		x = resize_tensor(x,size)
	else:
		# slightly downsample, patch size 90->64 is same fraction as 1024,1536->728,1092
		if oneshot:
			x = resize_tensor(x,(728,1096))
		else:
			x = resize_tensor(x,(64,64))

	# used for autoencoder going from grey to colour images to exaggerate difference in colour between maggots, dirt etc.
	if contrast:
		x = tf.image.adjust_contrast(x,1.5)
	
	return x / 255.


# get latent vec from image patch 
def patch_to_lvec(x,model,lsize,oneshot=True,pool_size=2):
	try:
		[_,_,lat],_ = model(x, training=False)
		size = int(lat.numpy().shape[1])
	except:
		lat,_ = model(x, training=False)
		size = int(lat.numpy().shape[1])
	
	if oneshot: # if whole image, pool by pool_size, then reshape to [X,lsize] where X will depend on the pool size
		lat = keras.layers.AveragePooling2D(pool_size)(lat)
		sh = lat.shape
		lat = tf.reshape(lat,[sh[1]*sh[2],lsize])
	else: # if a patch, pool so lvec is 1x1xlsize, then Flatten to [,lsize]
		lat = keras.layers.AveragePooling2D(pool_size=4)(lat) #4x4xL -> 1x1xL for patch
		sh = lat.shape
		lat = keras.layers.Flatten()(lat)
	return lat.numpy(), sh


def halfsize(x):
	return cv2.resize(x,(0,0),fx=0.5,fy=0.5)


def get_row_col_lengths(im,strides,patch_size):
	row_length = int(np.ceil(im.shape[1] / strides) - np.floor(patch_size / strides) )
	col_length = int(np.ceil(im.shape[0] / strides) - np.floor(patch_size / strides) + 1)

	return row_length, col_length


def row2square(X,row_length):
	n_rows = X.shape[0] / row_length
	X = np.split(X,n_rows,axis=0)
	X = np.expand_dims(X,0)
	return np.concatenate(X,axis=0)


# get image patches from image
def get_patches(image, patch_size=90, strides=90, aug=True): 
	tmp = []
	for i in range( int(np.ceil(image.shape[0] / strides) - np.floor(patch_size / strides)) ): # row idx
		for j in range( int(np.ceil(image.shape[1] / strides) - np.floor(patch_size / strides)) ): # col idx
			patch = image[(strides*i):(strides*i)+patch_size,(strides*j):(strides*j)+patch_size] # get patch
			if patch.shape[0] != patch_size: # if it's too short, i.e. patch goes off bottom edge
				if patch.shape[1] != patch_size: # if it's too thin, i.e. patch goes off right edge
					patch = image[-patch_size:,-patch_size:] # get new patch at right-bottom corner of correct size
				else:
					patch = image[-patch_size:,(strides*j):(strides*j)+patch_size] # get new patch at bottom edge of correct size
			else:
				if patch.shape[1] != patch_size:
					patch = image[(strides*i):(strides*i)+patch_size,-patch_size:] # get new patch at right edge of correct size
			
			# hard coded for patches to be size 90 at the moment, change if necessary
			if patch_size != 90:
				patch = cv2.resize(patch,(patch_size,patch_size))
			if aug == True:
				patch = rotate_and_flip(patch)
				assert (patch.shape == (8,patch_size,patch_size)) or (patch.shape == (8,patch_size,patch_size,3))

			if (len(patch.shape) == 2):
				patch = np.expand_dims(patch,axis=0)
			
			tmp.append(patch)
	
	patch_stack = np.concatenate(tmp,axis=0)
	return patch_stack


# get heatmap values by finding distance between test image latent vecs and reference latent vecs
def compare_descriptors(lvec,fvec,dvec):

	def get_dist(x,y,method='cos'):
		if method == 'euc':
			distance_matrix = sklearn.metrics.pairwise.euclidean_distances(x,y,squared=True)
		elif method == 'cos':
			distance_matrix = sklearn.metrics.pairwise.cosine_distances(x,y)
		return distance_matrix.diagonal()

	v1c = get_dist(lvec,dvec,'cos')
	v2c = get_dist(lvec,fvec,'cos')

	v1e = get_dist(lvec,dvec,'euc')
	v2e = get_dist(lvec,fvec,'euc')

	# how close is lvec to food lvec compared to dirt lvec:
	# 0 means v1c = 0, v2c > 0 so patch is dirt
	# 1 means v2c = 0 so patch is food
	v5c = v1c/(v2c+v1c)
	v5e = v1e/(v2e+v1e)
	
	return v5c


def eq(x,a,b,bump,off=1):
	if bump:
		return (off*((1-(1-x)**a)**(1/b)) + (1-off))
	else:
		return x*(off*((1-(1-x)**a)**(1/b)) + (1-off))


def covar_image(im1,im2,fsize=9,fac=6):
	im1 = cv2.resize(im1,(0,0),fx=0.1,fy=0.1)
	im2 = cv2.resize(im2,(0,0),fx=0.1,fy=0.1)

	h,w = im1.shape
	pad = int(0.5*(fsize-1))
	row_length = w-fsize+1

	covs=[]
	for i in range(h-fsize+1):
		for j in range(w-fsize+1):
			patch1 = im1[i:i+fsize-1,j:j+fsize-1]
			patch2 = im2[i:i+fsize-1,j:j+fsize-1]
			m1 = np.mean(patch1)
			m2 = np.mean(patch2)
			patch1 = patch1 / m1
			patch2 = patch2 / m2
			cov = np.mean((patch1-patch2)**2)
			covs.append(cov)

	X = row2square(scale(np.array(covs),uint_scale=False),row_length=row_length)
	X = cv2.copyMakeBorder(X, pad, pad, pad, pad, cv2.BORDER_CONSTANT)
	X = cv2.resize(X,(73,49))
	
	return grey2rgb(X)


# convert image to row of lvecs for each patch
def im_to_lvec(im, model, lsize, oneshot, strides=20, patch_size=90):
	if oneshot:
		x = prepro(im,oneshot=oneshot)
		return patch_to_lvec(x,model,lsize,oneshot=True)
	else:
		patches = get_patches(im, patch_size=patch_size, strides=strides,aug=False)
		xs = prepro(patches,oneshot=oneshot)
		db = tf.data.Dataset.from_tensor_slices((xs))
		db = db.batch(64)
		lvecs=[]
		for step, x in enumerate(db):
			lat_vec_batch,_ = patch_to_lvec(x, model, lsize, oneshot=False, pool_size=4)
			lvecs.append(lat_vec_batch)
		lvecs = np.concatenate(lvecs,axis=0)
		
		return lvecs, lvecs.shape 


# needs work - determine food amount from image patch. Again best way to see what the function is doing is going to
# desmos.com/calculator and comparing below function with y=x
def get_food_amount(hm,fac):
	return np.mean(hm+(1.5*(hm**fac)))


def show(x):
	cv2.imshow('',x)
	cv2.waitKey(0)
	cv2.destroyAllWindows()


def get_heatmap(im, prev_im, model, ref_vecs, pool=True):
	
	# Have left in (commented out) funcitonality for using correlation between image and previous image to create a mask which ignores areas of the image
	# that arent moving. however this is no longer used as results without are satisfactory and doesn't have the problem of not detecting partially churned up
	# food that isn't moving

	fvec,dvec = ref_vecs
	patch_size = 90
	strides = 60
	oneshot = False
	lsize = 10
	
	im = halfsize(im)
	new_time = time.time()
	lvec,sh = im_to_lvec(im,model,lsize,oneshot,strides)
	row_length,col_length = get_row_col_lengths(im,strides,patch_size)
	hm = compare_descriptors(lvec,fvec,dvec)

	if prev_im is not None:
		#prev_im = halfsize(prev_im)
		#c = covar_image(im,prev_im)
		#c[hm >= 0.75] = 1
		#c = ndimage.gaussian_filter(c,sigma=2)
		#c = c / np.max(c)
		#c = cv2.resize(c,(im.shape[1],im.shape[0]))
		#hm = hm * c
		pass

	hm = cv2.resize(hm,(im.shape[1]-int(patch_size/2),im.shape[0]-int(patch_size/2)),interpolation=cv2.INTER_NEAREST)
	hm = cv2.copyMakeBorder(hm, 45, 45, 45, 45, cv2.BORDER_CONSTANT)
	hm = cv2.resize(hm,(im.shape[1],im.shape[0]),interpolation=cv2.INTER_NEAREST)
	food_amount = get_food_amount(hm,fac=5)

	return hm, food_amount